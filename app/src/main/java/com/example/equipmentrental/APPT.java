package com.example.equipmentrental;

public class APPT {
    private String date;
    private String time;
    private String id;
    private String reason;
    private String apptID;

    public APPT() {
    }

    public APPT(String date, String time, String id, String reason, String apptID) {
        this.date = date;
        this.time = time;
        this.id = id;
        this.reason = reason;
        this.apptID = apptID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getApptID() {
        return apptID;
    }

    public void setApptID(String apptID) {
        this.apptID = apptID;
    }
}
