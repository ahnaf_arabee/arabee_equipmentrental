package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import ViewHolder.RentalViewHolder;

public class MainActivity14 extends AppCompatActivity {
    Context context;



    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<APPT, ApptViewHolder> adapter;
    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main14);
        database=FirebaseDatabase.getInstance();
        reference= database.getReference("Clinic");

        recyclerView = (RecyclerView)findViewById(R.id.rv_appt);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        showList();
    }

    private void showList() {
        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<APPT>()
                .setQuery(reference,APPT.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<APPT, ApptViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ApptViewHolder holder, int position, @NonNull APPT model) {
                holder.txtdate.setText(model.getDate());
                holder.txttime.setText(model.getTime());
                holder.txtid.setText(model.getId());
                holder.txtreason.setText(model.getReason());
                holder.txtapptid.setText(model.getApptID());


                holder.txtdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(),"successful",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity15.class);
                        intent.putExtra("dateA",model.getDate());
                        intent.putExtra("timeA",model.getTime());
                        intent.putExtra("idA",model.getId());
                        intent.putExtra("reasonA",model.getReason());
                        intent.putExtra("apptIDA",model.getApptID());
                        startActivity(intent);


                    }
                });


            }

            @NonNull
            @Override
            public ApptViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.appointment,viewGroup,false);
                return new ApptViewHolder(view);
            }
        };
        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
}