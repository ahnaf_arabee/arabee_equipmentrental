package com.example.equipmentrental;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Clinic_Adapter extends RecyclerView.Adapter<Clinic_Adapter.ViewHolder> {
    Context context;
    List<Clinic_details> clinic_listList;
    private AdapterView.OnItemClickListener mOnTtemClickListener;

    public Clinic_Adapter(Context context, List<Clinic_details> clinic_listList) {
        this.context = context;
        this.clinic_listList = clinic_listList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_clinic_list_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Clinic_Adapter.ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
