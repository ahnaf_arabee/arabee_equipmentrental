package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity7 extends AppCompatActivity {

    TextView sportsName;
    TextView sportsEquipment;
    TextView sportsQuantity;
    TextView sportsRent;
    TextView sportsReturn;
    TextView sportsPay;
    TextView rentId;
    private Button button3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);


        sportsName = (TextView) findViewById(R.id.sportsname1);
        sportsEquipment = (TextView) findViewById(R.id.sportsequipment1);
        sportsQuantity = (TextView) findViewById(R.id.sportsquantity1);
        sportsRent= (TextView) findViewById(R.id.sportsrent1);
        sportsReturn = (TextView) findViewById(R.id.sportsreturn1);
        sportsPay = (TextView) findViewById(R.id.sportspay1);
        rentId = (TextView) findViewById(R.id.rentid1);

        String sports1 = getIntent().getStringExtra("key1");
        String sports2 = getIntent().getStringExtra("key2");
        String sports3 = getIntent().getStringExtra("key3");
        String sports4 = getIntent().getStringExtra("key4");
        String sports5 = getIntent().getStringExtra("key5");
        String sports6 = getIntent().getStringExtra("key6");
        String sports7 = getIntent().getStringExtra("key7");


        sportsName.setText(sports1);
        sportsEquipment.setText(sports2);
        sportsQuantity.setText(sports3);
        sportsRent.setText(sports4);
        sportsReturn .setText(sports5);
        sportsPay.setText(sports6);
        rentId.setText(sports7);


        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("Rental").orderByChild("rentID").equalTo(sports7);

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot snapshot1: snapshot.getChildren()){
                            snapshot1.getRef().removeValue();
                            Intent intent = new Intent(MainActivity7.this, MainActivity8.class);
                            MainActivity7.this.startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {


                    }
                });
            }
        });
    }

}