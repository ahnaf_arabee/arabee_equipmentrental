package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.lang.reflect.Member;
import java.util.HashMap;


public class MainActivity9 extends AppCompatActivity {

    EditText txtsports,txtequipment,txtquantity,txtpayment;
    Button btnsave;
    DatabaseReference reff;
    Eqrental eqrental;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main9);
        txtsports=(EditText)findViewById(R.id.sports0);
        txtequipment=(EditText)findViewById(R.id.equipment0);
        txtquantity=(EditText)findViewById(R.id.quantity0);
        txtpayment=(EditText)findViewById(R.id.payment0);
        btnsave=(Button)findViewById(R.id.button0);
        eqrental =new Eqrental();

        reff = FirebaseDatabase.getInstance().getReference().child("EQ_rental");
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           String sport = txtsports.getText().toString();
                String equip = txtequipment.getText().toString();
                String quan = txtquantity.getText().toString();
                String pay = txtpayment.getText().toString();
                eqrental.setSports(sport);
                eqrental.setEquipment(equip);
                eqrental.setQuantity(quan);
                eqrental.setPayment(pay);
                reff.push().setValue(eqrental);


            }
        });
    }
}