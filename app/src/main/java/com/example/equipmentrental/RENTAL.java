package com.example.equipmentrental;

public class RENTAL {
    private String selectsports;
    private String rentdate;
    private String returndate;
    private String selectequipment;
    private String selectquantity;
    private String payment;

    public RENTAL() {
    }

    public RENTAL(String selectsports, String rentdate, String returndate, String selectequipment, String selectquantity, String payment) {
        this.selectsports = selectsports;
        this.rentdate = rentdate;
        this.returndate = returndate;
        this.selectequipment = selectequipment;
        this.selectquantity = selectquantity;
        this.payment = payment;
    }

    public String getSelectsports() {
        return selectsports;
    }

    public void setSelectsports(String selectsports) {
        this.selectsports = selectsports;
    }

    public String getRentdate() {
        return rentdate;
    }

    public void setRentdate(String rentdate) {
        this.rentdate = rentdate;
    }

    public String getReturndate() {
        return returndate;
    }

    public void setReturndate(String returndate) {
        this.returndate = returndate;
    }

    public String getSelectequipment() {
        return selectequipment;
    }

    public void setSelectequipment(String selectequipment) {
        this.selectequipment = selectequipment;
    }

    public String getSelectquantity() {
        return selectquantity;
    }

    public void setSelectquantity(String selectquantity) {
        this.selectquantity = selectquantity;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
