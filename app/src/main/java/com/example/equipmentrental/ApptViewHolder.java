package com.example.equipmentrental;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ApptViewHolder extends RecyclerView.ViewHolder {
    public TextView txtdate;
    public TextView txttime;
    public TextView txtid;
    public TextView txtreason;
    public TextView txtapptid;

    public ApptViewHolder(@NonNull View itemView) {
        super(itemView);
        txtdate =(TextView)itemView.findViewById(R.id.date_textView);
        txttime =(TextView)itemView.findViewById(R.id.time_textView);
        txtid =(TextView)itemView.findViewById(R.id.id_textView);
        txtreason =(TextView)itemView.findViewById(R.id.reason_textView);
        txtapptid=(TextView)itemView.findViewById(R.id.apptID_textView);


    }
}


