package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity2 extends AppCompatActivity {

    Spinner spinner1,spinner2,spinner3,spinner4;
    Spinner selectSports,selectEquipment,selectQuantity,payment;
    EditText rentdate,returndate,rentid;
    TextView textView,textView1,textView2,textView3;




    DatabaseReference reference;
    DatabaseReference reference1;
    Rentsupport rentsupport;
   //int maxid = 0;

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        selectSports = findViewById(R.id.spinner1);
        selectEquipment = findViewById(R.id.spinner2);
        selectQuantity = findViewById(R.id.spinner3);
        payment = findViewById(R.id.spinner4);
        textView= findViewById(R.id.sportsA);
        textView1= findViewById(R.id.equipmentA);
        textView2= findViewById(R.id.quantityA);
        textView3= findViewById(R.id.paymentA);


        spinner1 = (Spinner)findViewById(R.id.spinner1);
        spinner2 = (Spinner)findViewById(R.id.spinner2);
        spinner3 = (Spinner)findViewById(R.id.spinner3);
        spinner4 = (Spinner)findViewById(R.id.spinner4);
        rentdate = findViewById(R.id.rentDate);
        returndate = findViewById(R.id.returnDate);

        button = (Button) findViewById(R.id.button);

        rentsupport = new Rentsupport();

       reference = FirebaseDatabase.getInstance().getReference().child("EQ_rental");
        reference1 = FirebaseDatabase.getInstance().getReference().child("Rental");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String equip = selectEquipment.getSelectedItem().toString();
                String rent =  rentdate.getText().toString();
                String returnd =returndate.getText().toString();
                String id = "UTM"+equip+rent+returnd;

                rentsupport.setSelectsports(textView.getText().toString());
                rentsupport.setSelectquantity(textView1.getText().toString());
                rentsupport.setSelectequipment(textView2.getText().toString());
                rentsupport.setPayment(textView3.getText().toString());
                rentsupport.setRentdate(rentdate.getText().toString());
                rentsupport.setReturndate(returndate.getText().toString());
                rentsupport.setRentID(id);

                //reference1.child(String.valueOf(maxid+1)).setValue(rentsupport);
                reference1.push().setValue(rentsupport);
                openActivity3();

            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        rentdate =(EditText) findViewById(R.id.rentDate);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                 updateLabel();
            }
            private void updateLabel(){
                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                rentdate.setText(sdf.format(myCalendar.getTime()));
            }
        };
        rentdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MainActivity2.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        final Calendar mCalendar = Calendar.getInstance();
        returndate =(EditText) findViewById(R.id.returnDate);
        DatePickerDialog.OnDateSetListener mdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateLabel();
            }
            private void updateLabel(){
                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                returndate.setText(sdf.format(myCalendar.getTime()));
            }
        };
        returndate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MainActivity2.this,mdate,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> Map =new ArrayList<String>();

                for (DataSnapshot MapSnapshot:dataSnapshot.getChildren()){
                   String sports = MapSnapshot.child("sports").getValue(String.class);
                   Map.add(sports);
                }
                Spinner MapSpinner = (Spinner) findViewById(R.id.spinner1);
                ArrayAdapter<String> MapAdapter = new ArrayAdapter<String>(MainActivity2.this,android.R.layout.simple_spinner_dropdown_item,Map);
                MapAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                MapSpinner.setAdapter(MapAdapter);
                MapSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String test = parent.getItemAtPosition(position).toString();
                        textView.setText(test);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> Equip =new ArrayList<String>();
             for(DataSnapshot EquipSnapshot:dataSnapshot.getChildren()){
                 String equip = EquipSnapshot.child("equipment").getValue(String.class);
                 Equip.add(equip);
             }
             Spinner equipSpinner = (Spinner) findViewById(R.id.spinner2);
             ArrayAdapter<String> EquipAdapter = new ArrayAdapter<String>(MainActivity2.this, android.R.layout.simple_spinner_dropdown_item,Equip);
             equipSpinner.setAdapter(EquipAdapter);

             equipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                 @Override
                 public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                     String equip1 = parent.getItemAtPosition(position).toString();
                     textView1.setText(equip1);
                 }

                 @Override
                 public void onNothingSelected(AdapterView<?> parent) {

                 }
             });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> Quan =new ArrayList<String>();
                for(DataSnapshot QuanSnapshot:dataSnapshot.getChildren()){
                    String quan = QuanSnapshot.child("quantity").getValue(String.class);
                    Quan.add(quan);
                }
                Spinner quanSpinner = (Spinner) findViewById(R.id.spinner3);
                ArrayAdapter<String> QuanAdapter = new ArrayAdapter<String>(MainActivity2.this, android.R.layout.simple_spinner_dropdown_item,Quan);
                quanSpinner.setAdapter(QuanAdapter);

                quanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String quan1 = parent.getItemAtPosition(position).toString();
                        textView2.setText(quan1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> Pay =new ArrayList<String>();
                for(DataSnapshot PaySnapshot:dataSnapshot.getChildren()){
                    String pay = PaySnapshot.child("payment").getValue(String.class);
                    Pay.add(pay);
                }
                Spinner paySpinner = (Spinner) findViewById(R.id.spinner4);
                ArrayAdapter<String> PayAdapter = new ArrayAdapter<String>(MainActivity2.this, android.R.layout.simple_spinner_dropdown_item,Pay);
                paySpinner.setAdapter(PayAdapter);

                paySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String pay1 = parent.getItemAtPosition(position).toString();
                        textView3.setText(pay1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }





    public void openActivity3(){
        Intent intent = new Intent(this, MainActivity3.class);
        startActivity(intent);
    }


}