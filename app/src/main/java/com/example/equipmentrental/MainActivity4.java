package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import ViewHolder.RentalViewHolder;

public class MainActivity4 extends AppCompatActivity{
    private static final String TAG ="MainActivity4";
    EditText searchdate;
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    ArrayList<String> selectsportsList;
    ArrayList<String> selectquantityList;
    ArrayList<String> selectequipmentList;
    ArrayList<String> rentdateList;
    ArrayList<String> returndateList;
    ArrayList<String> paymentList;
    ArrayList<String> rentidList;

    SearchAdapter searchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        recyclerView =(RecyclerView) findViewById(R.id.recycler_view);
        searchdate = (EditText) findViewById(R.id.search_date);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));

        selectsportsList = new ArrayList<>();
        selectequipmentList = new ArrayList<>();
        selectquantityList = new ArrayList<>();
        rentdateList = new ArrayList<>();
        returndateList = new ArrayList<>();
        paymentList = new ArrayList<>();
        rentidList= new ArrayList<>();

        searchdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()){
                    setAdapter(s.toString());

                }

            }
        });



    }

    private void setAdapter(final String searchedString){
        int counter = 0;

        databaseReference.child("Rental").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot:dataSnapshot.getChildren()){

                    String uid = snapshot.getKey();
                    String selectsports = snapshot.child("selectsports").getValue(String.class);
                    String selectequipment = snapshot.child("selectequipment").getValue(String.class);
                    String selectquantity = snapshot.child("selectquantity").getValue(String.class);
                    String rentdate = snapshot.child("rentdate").getValue(String.class);
                    String returndate = snapshot.child("returndate").getValue(String.class);
                    String payment = snapshot.child("payment").getValue(String.class);
                    String rentid = snapshot.child("rentID").getValue(String.class);

                    //Toast.makeText(MainActivity4.this, selectsports, Toast.LENGTH_SHORT).show();

                    if (selectsports.toLowerCase().contains(searchedString.toLowerCase())){
                        selectsportsList.add(selectsports);
                        selectequipmentList.add(selectequipment);
                        selectquantityList.add(selectquantity);
                        rentdateList.add(rentdate);
                        returndateList.add(returndate);
                        paymentList.add(payment);
                        rentidList.add(rentid);

                    } else if (rentdate.toLowerCase().contains(searchedString.toLowerCase())){

                        selectsportsList.add(selectsports);
                        selectequipmentList.add(selectequipment);
                        selectquantityList.add(selectquantity);
                        rentdateList.add(rentdate);
                        returndateList.add(returndate);
                        paymentList.add(payment);
                        rentidList.add(rentid);


                    }
                    else{

                    }

                    //Toast.makeText(getApplicationContext(), String.valueOf(selectsportsList.size()), Toast.LENGTH_SHORT).show();
                }

                searchAdapter= new SearchAdapter(MainActivity4.this,selectsportsList,selectequipmentList,selectquantityList,rentdateList,returndateList,paymentList,rentidList);
                recyclerView.setAdapter(searchAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        /*databaseReference.child("Rental").addValueEventListener(new ValueEventListener()  {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for (DataSnapshot snapshot:dataSnapshot.getChildren()){

                    String uid = snapshot.getKey();
                    String selectsports = snapshot.child("selectsports").getValue(String.class);
                    String selectequipment = snapshot.child("selectequipment").getValue(String.class);
                    String selectquantity = snapshot.child("selectquantity").getValue(String.class);
                    String rentdate = snapshot.child("rentdate").getValue(String.class);
                    String returndate = snapshot.child("returndate").getValue(String.class);
                    String payment = snapshot.child("payment").getValue(String.class);

                    if (selectsports.toLowerCase().contains(searchedString.toLowerCase())){
                        selectsportsList.add(selectsports);
                        selectequipmentList.add(selectequipment);
                        selectquantityList.add(selectquantity);
                        rentdateList.add(rentdate);
                        returndateList.add(returndate);
                        paymentList.add(payment);

                    } else if (rentdate.toLowerCase().contains(searchedString.toLowerCase())){

                        selectsportsList.add(selectsports);
                        selectequipmentList.add(selectequipment);
                        selectquantityList.add(selectquantity);
                        rentdateList.add(rentdate);
                        returndateList.add(returndate);
                        paymentList.add(payment);


                    }
                    else{

                    }

                    //Toast.makeText(getApplicationContext(), String.valueOf(selectsportsList.size()), Toast.LENGTH_SHORT).show();
                }

              searchAdapter= new SearchAdapter(MainActivity4.this, selectsportsList,selectequipmentList,selectquantityList,rentdateList,returndateList,paymentList);
              recyclerView.setAdapter(searchAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
    }




    /*public void openActivity5(){
        Intent intent = new Intent(this, MainActivity5.class);
        startActivity(intent);
    }*/


    }
