package com.example.equipmentrental;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity6 extends AppCompatActivity {

int position;


    private Button button4;

    TextView sportsName;
    TextView sportsEquipment;
    TextView sportsQuantity;
    TextView sportsRent;
    TextView sportsReturn;
    TextView sportsPay;
    TextView rentId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        sportsName = (TextView) findViewById(R.id.sportsname);
        sportsEquipment = (TextView) findViewById(R.id.sportsequipment);
        sportsQuantity = (TextView) findViewById(R.id.sportsquantity);
        sportsRent= (TextView) findViewById(R.id.sportsrent);
        sportsReturn = (TextView) findViewById(R.id.sportsreturn);
        sportsPay = (TextView) findViewById(R.id.sportspay);
        rentId = (TextView) findViewById(R.id.rentid);

        String sportsN = getIntent().getStringExtra("sportsName");
        String sportsM = getIntent().getStringExtra("equipmentName");
        String sportsO = getIntent().getStringExtra("quantity");
        String sportsP = getIntent().getStringExtra("rent");
        String sportsQ = getIntent().getStringExtra("return");
        String sportsR = getIntent().getStringExtra("pay");
        String sportsS = getIntent().getStringExtra("rentid");


        sportsName.setText(sportsN);
        sportsEquipment.setText(sportsM);
        sportsQuantity.setText(sportsO);
        sportsRent.setText(sportsP);
        sportsReturn .setText(sportsQ);
        sportsPay.setText(sportsR);
        rentId.setText(sportsS);

        String text1 = sportsName.getText().toString();
        String text2 = sportsEquipment.getText().toString();
        String text3 = sportsQuantity.getText().toString();
        String text4 = sportsRent.getText().toString();
        String text5 = sportsReturn.getText().toString();
        String text6 = sportsPay.getText().toString();
        String text7 = rentId.getText().toString();



        Toast.makeText(MainActivity6.this, text1, Toast.LENGTH_SHORT).show();

        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity6.this,MainActivity7.class);
                intent.putExtra("key1",text1);
                intent.putExtra("key2",text2);
                intent.putExtra("key3",text3);
                intent.putExtra("key4",text4);
                intent.putExtra("key5",text5);
                intent.putExtra("key6",text6);
                intent.putExtra("key7",text7);

                startActivity(intent);
            }
        });


    }

}