package com.example.equipmentrental;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

   Context context;
    ArrayList<String> selectsportsList;
    ArrayList<String> selectquantityList;
    ArrayList<String> selectequipmentList;
    ArrayList<String> rentdateList;
    ArrayList<String> returndateList;
    ArrayList<String> paymentList;
    ArrayList<String> rentidList;

    class SearchViewHolder extends RecyclerView.ViewHolder  {
        TextView selectsports,selecteqipment,selectquantity,rentdate,returndate,payment,rentid;

        public SearchViewHolder(@NonNull View itemView){
            super(itemView);
            selectsports= itemView.findViewById(R.id.select_sports);
            selecteqipment = itemView.findViewById(R.id.select_equipment);
            selectquantity = itemView.findViewById(R.id.select_quantity);
            rentdate = itemView.findViewById(R.id.rent_date);
            returndate = itemView.findViewById(R.id.return_date);
            payment= itemView.findViewById(R.id.pay_ment);
            rentid = itemView.findViewById(R.id.rent_id);
        }
    }

    public SearchAdapter(Context context, ArrayList<String> selectsportsList, ArrayList<String> selectequipmentList, ArrayList<String> selectquantityList, ArrayList<String> rentdateList, ArrayList<String> returndateList, ArrayList<String> paymentList,ArrayList<String> rentidList) {
        this.context = context;
        this.selectsportsList = selectsportsList;
        this.selectquantityList = selectquantityList;
        this.selectequipmentList = selectequipmentList;
        this.rentdateList = rentdateList;
        this.returndateList = returndateList;
        this.paymentList = paymentList;
        this.rentidList = rentidList;
    }

    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.search_list_items,parent,false);

        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        holder.selectsports.setText(selectsportsList.get(position));
        holder.selecteqipment.setText(selectequipmentList.get(position));
        holder.selectquantity.setText(selectquantityList.get(position));
        holder.rentdate.setText(rentdateList.get(position));
        holder.returndate.setText(returndateList.get(position));
        holder.payment.setText(paymentList.get(position));
        holder.rentid.setText(rentidList.get(position));

        holder.selectsports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, MainActivity6.class);
                intent.putExtra("sportsName", selectsportsList.get(position));
                intent.putExtra("equipmentName", selectequipmentList.get(position));
                intent.putExtra("quantity", selectquantityList.get(position));
                intent.putExtra("rent", rentdateList.get(position));
                intent.putExtra("return", returndateList.get(position));
                intent.putExtra("pay", paymentList.get(position));
                intent.putExtra("rentid", rentidList.get(position));

                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return this.selectsportsList.size();
    }
}
