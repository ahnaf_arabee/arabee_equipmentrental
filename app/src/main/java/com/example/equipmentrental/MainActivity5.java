package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ViewHolder.RentalViewHolder;

public class MainActivity5 extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<RENTAL, RentalViewHolder> adapter;
    FirebaseDatabase database;
    DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        database=FirebaseDatabase.getInstance();
        reference= database.getReference("Rental");

        recyclerView = (RecyclerView)findViewById(R.id.rv_1);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        showList();
    }

    private void showList()
    {
        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<RENTAL>()
                .setQuery(reference,RENTAL.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<RENTAL, RentalViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RentalViewHolder holder, int position, @NonNull RENTAL model) {
            holder.txtselectsports.setText(model.getSelectsports());
            holder.txtrentdate.setText(model.getRentdate());
            holder.txtreturndate.setText(model.getReturndate());
            holder.txtselectequipment.setText(model.getSelectequipment());
            holder.txtselectquantity.setText(model.getSelectquantity());
            holder.txtpayment.setText(model.getPayment());
            }

            @NonNull
            @Override
            public RentalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_rental,viewGroup,false);
                return new RentalViewHolder(view);
            }
        };
        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
}