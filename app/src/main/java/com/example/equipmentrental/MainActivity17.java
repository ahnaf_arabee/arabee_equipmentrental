package com.example.equipmentrental;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity17 extends AppCompatActivity {
    EditText txtdate,txtdr,txtstatus,txttime;
    Button btnstore;
    DatabaseReference reference;
    Clinic_details clinic_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main17);
        txtdate=(EditText)findViewById(R.id.clinic_date);
        txtdr=(EditText)findViewById(R.id.clinic_dr);
        txtstatus=(EditText)findViewById(R.id.clinic_status);
        txttime=(EditText)findViewById(R.id.clinic_time);
        btnstore= (Button)findViewById(R.id.button18);
        clinic_details= new Clinic_details();
        reference= FirebaseDatabase.getInstance().getReference().child("Clinic_Details");
        btnstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date= txtdate.getText().toString();
                String dr= txtdr.getText().toString();
                String status= txtstatus.getText().toString();
                String time= txttime.getText().toString();
                clinic_details.setDate(date);
                clinic_details.setDr(dr);
                clinic_details.setStatus(status);
                clinic_details.setTime(time);
                reference.push().setValue(clinic_details);


            }
        });
    }
}