package com.example.equipmentrental;

public class Clinic_details {
    private String Date;
    private String Dr;
    private String Status;
    private String Time;

    public Clinic_details() {
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
      this.Date = date;
    }

    public String getDr() {
        return Dr;
    }

    public void setDr(String dr) {
        this.Dr = dr;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        this.Time = time;
    }
}
