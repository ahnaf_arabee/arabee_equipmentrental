package com.example.equipmentrental;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity15 extends AppCompatActivity {
    TextView showDate;
    TextView showTime;
    TextView showId;
    TextView showReason;
    TextView showApptid;
    private Button delete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main15);

        showDate = (TextView) findViewById(R.id.showdate);
        showTime = (TextView) findViewById(R.id.showtime);
        showId = (TextView) findViewById(R.id.showid);
        showReason = (TextView) findViewById(R.id.showreason);
        showApptid = (TextView) findViewById(R.id.showapptid);

        String apptA = getIntent().getStringExtra("dateA");
        String apptB = getIntent().getStringExtra("timeA");
        String apptC = getIntent().getStringExtra("idA");
        String apptD = getIntent().getStringExtra("reasonA");
        String apptE = getIntent().getStringExtra("apptIDA");

        showDate.setText(apptA);
        showTime.setText(apptB);
        showId.setText(apptC);
        showReason.setText(apptD);
        showApptid.setText(apptE);

        delete = (Button) findViewById(R.id.button14);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("Clinic").orderByChild("apptID").equalTo(apptE);

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot snapshot1: snapshot.getChildren()){
                            snapshot1.getRef().removeValue();
                            Intent intent = new Intent(MainActivity15.this, MainActivity16.class);
                            MainActivity15.this.startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

    }
}