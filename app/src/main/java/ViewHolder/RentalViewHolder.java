package ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.equipmentrental.R;

public class RentalViewHolder extends RecyclerView.ViewHolder {
    public TextView txtselectsports;
    public TextView txtrentdate;
    public TextView txtreturndate;
    public TextView txtselectequipment;
    public TextView txtselectquantity;
    public TextView txtpayment;
    public RentalViewHolder(@NonNull View itemView) {
        super(itemView);
        txtselectsports = (TextView)itemView.findViewById(R.id.selectsports_textView);
        txtrentdate = (TextView)itemView.findViewById(R.id.rentdate_textView);
        txtreturndate = (TextView)itemView.findViewById(R.id.returndate_textView);
        txtselectequipment = (TextView)itemView.findViewById(R.id.selectequipment_textView);
        txtselectquantity = (TextView)itemView.findViewById(R.id.selectquantity_textView);
        txtpayment = (TextView)itemView.findViewById(R.id.payment_textView);
    }
}
